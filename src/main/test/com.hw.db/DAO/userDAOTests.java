package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class userDAOTests {

    @Test
    @DisplayName("Change user fields test: all fields")
    void test_UserChangeTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", "admin@admin.ru", "namefull", "huabout");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user fields test: except email")
    void test_UserChangeTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", null, "namefull", "huabout");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user one field test: email")
    void test_UserChangeTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", "admin@admin.ru", null, null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user fields test: except full name")
    void test_UserChangeTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", "admin@admin.ru", null, "huabout");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user one field test: fullname")
    void test_UserChangeTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", null, "namefull", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user fields test: except about")
    void test_UserChangeTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", "admin@admin.ru", "namefull", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user one field test: about")
    void test_UserChangeTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", null, null, "huabout");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change user: no changes")
    void test_UserChangeTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("huname", null, null, null);
        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }
}
package com.hw.db.DAO;


import com.hw.db.models.Post;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

public class postDAOTests {

    @Test
    @DisplayName("New author test")
    void test_SetPostTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("Newhuauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("New message test")
    void test_SetPostTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("huauthor", new Timestamp(1), "forum", "newhuessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("New time Test")
    void test_SetPostTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("huauthor", new Timestamp(2), "forum", "huessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("New author & time & message test")
    void test_SetPostTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("newhuauthor", new Timestamp(2), "forum", "newhuessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("New author & message test")
    void test_SetPostTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("newhuauthor", new Timestamp(1), "forum", "newhuessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("New author & time test")
    void test_SetPostTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("newhuauthor", new Timestamp(2), "forum", "huessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("New time & message test")
    void test_SetPostTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("huauthor", new Timestamp(2), "forum", "newhuessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("No updates test")
    void test_SetPostTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        Post newPost = new Post("huauthor", new Timestamp(1), "forum", "huessage", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1));
        verifyNoMoreInteractions(mockJdbc);

    }

}
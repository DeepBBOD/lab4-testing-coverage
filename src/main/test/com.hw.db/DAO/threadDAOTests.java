package com.hw.db.DAO;

import com.hw.db.models.Thread;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class threadDAOTests {

    @Test
    @DisplayName("Descending order test")
    void test_TreeSort1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ? " + " AND branch < (SELECT branch  FROM posts WHERE id = ?) " + " ORDER BY branch" + " DESC " + " LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Ascending order test")
    void test_TreeSort2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ? " + " AND branch > (SELECT branch  FROM posts WHERE id = ?) " + " ORDER BY branch" + " LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }
}